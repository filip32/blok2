﻿using Common;
using System;
using System.Security;
using System.Collections.Generic;
using System.Linq;

namespace CredentialsStore
{
    public class AccountManagement : IAccountManagement
    {
        public bool CreateAccount(string username, byte[] password)
        {
            bool ret = true;
            User user = new User(username, password);

            if (DataUsers.Users.ContainsKey(username))
            {
                ret = false;
                Console.WriteLine("User with username [{0}] already exists!", username);
                throw new SecurityException($"User with username [{username}] already exists!");
            }
            else
            {
                DataUsers.Users.Add(username, user);
                Console.WriteLine("Created account [username = {0}]....", username);
                DataUsers.WriteToXML();
            }

            return ret;
        }

        public bool DeleteAccount(string username)
        {
            bool ret = true;

            if (DataUsers.Users.ContainsKey(username))
            {
                DataUsers.Users.Remove(username);
                Console.WriteLine("Deleted account [username = {0}]....", username);
                DataUsers.WriteToXML();
            }
            else
            {
                ret = false;
                Console.WriteLine("User with username [{0}] does not exists!", username);
                throw new SecurityException($"User with username [{username}] already exists!");
            }

            return ret;
        }

        public bool DisableAccount(string username)
        {
            bool ret = true;
            User user = null;

            if (DataUsers.Users.TryGetValue(username, out user))
            {
                user.IsDisabled = true;
                Console.WriteLine("Disabled account [username = {0}]....", username);
                DataUsers.WriteToXML();
            }
            else
            {
                ret = false;
                Console.WriteLine("User with username [{0}] does not exists!", username);
                throw new SecurityException($"User with username [{username}] already exists!");
            }

            return ret;
        }

        public bool EnableAccount(string username)
        {
            bool ret = true;
            User user = null;

            if (DataUsers.Users.TryGetValue(username, out user))
            {
                user.IsDisabled = false;
                Console.WriteLine("Enabled account [username = {0}]....", username);
                DataUsers.WriteToXML();
            }
            else
            {
                ret = false;
                Console.WriteLine("User with username [{0}] does not exists!", username);
                throw new SecurityException($"User with username [{username}] already exists!");
            }

            return ret;
        }
        
        public bool LockAccount(string username)
        {
            bool ret = true;
            User user = null;

            if (DataUsers.Users.TryGetValue(username, out user))
            {
                user.IsBlocked = true;
                user.AdminBlockedUser = true;
                user.CntForLocking = 0;
                Console.WriteLine("Locked account [username = {0}]....", username);
                DataUsers.WriteToXML();
            }
            else
            {
                ret = false;
                Console.WriteLine("User with username [{0}] does not exists!", username);
                throw new SecurityException($"User with username [{username}] already exists!");
            }

            return ret;
        }

        public List<User> GetAllUsers()
        {
            if (DataUsers.Users.Values.Count > 0)
                return DataUsers.Users.Values.ToList();
            else
                return null;
        }
    }
}
