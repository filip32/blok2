﻿using Common;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System.Windows.Forms;
using System;

namespace CredentialsStore
{
    public static class DataUsers
    {
        private static Dictionary<string, User> users = null;
        private static readonly object lockUsers = new object();
        private static string filename = GetPath() + @"\Users.xml";

        public static Dictionary<string, User> Users
        {
            get
            {
                if (users == null)
                {
                    lock (lockUsers)
                    {
                        if (users == null)
                        {
                            users = new Dictionary<string, User>();
                        }
                    }
                }
                return users;
            }
        }

        public static string GetPath()
        {
            string path = "";
            path = Path.GetDirectoryName(Application.ExecutablePath);
            path = path.Replace("CredentialsStore\\bin\\Debug", "");
            path = path + @"UsersData";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path;
        }

        public static void WriteToXML()
        {
            List<User> list = Users.Values.ToList();

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<User>));
                using (TextWriter writer = new StreamWriter(filename, false))
                {
                    serializer.Serialize(writer, list);
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("WriteToXML(): Exception:\n{0}", ex);
                throw ex;
            }
        }

        public static void ReadFromXML()
        {
            List<User> list = null;

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<User>));
                using (TextReader reader = new StreamReader(filename))
                {
                    list = (List<User>)serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("ReadFromXML(): Exception:\n{0}", ex);
                throw ex;
            }

            users = new Dictionary<string, User>();
            foreach(User u in list)
            {
                users.Add(u.Username, u);
            }
        }
    }
}
