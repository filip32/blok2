﻿using Common;
using System;

namespace CredentialsStore
{
    public class CheckUsers : ICheckUsers
    {
        private const short cntForLocking = 2;//cntForLocking in user too
        private const double lockedValue = 1;//lock
        private const double inActiveUser = 2;//disable

        public bool AuthenticateUser(string username, byte[] password)
        {
            bool ret = true;
            User user = null;
            
            if (DataUsers.Users.TryGetValue(username, out user))
            {
                if (user.AdminBlockedUser == false)
                {
                    if (DateTime.Compare(DateTime.Now, user.LockedTime) >= 0)
                    {
                        user.IsBlocked = false;
                        if (user.CntForLocking > 0)
                        {
                            if (CheckPassword(user, password))
                            {
                                user.IsAuthenticated = true;
                                user.CntForLocking = cntForLocking;
                                Console.WriteLine("User [{0}] is authenticated....", username);
                            }
                            else
                            {
                                user.CntForLocking--;
                                ret = false;
                                Console.WriteLine("User [{0}] is NOT authenticated!", username);
                            }
                        }
                        else
                        {
                            ret = false;
                            user.LockedTime = DateTime.Now.AddMinutes(lockedValue);
                            user.IsBlocked = true;
                            user.CntForLocking = cntForLocking;
                            Console.WriteLine("User [{0}] is NOW blocked....", username);
                        }
                    }
                    else
                    {
                        ret = false;
                        Console.WriteLine("User [{0}] is blocked....", username);
                    }
                }
                else
                {
                    ret = false;
                    Console.WriteLine("User [{0}] is blocked by admin or system!", username);
                }
            }
            else
            {
                ret = false;
                Console.WriteLine("User with username [{0}] does not exists!", username);
            }

            return ret;
        }

        public bool CheckIfDisabled(string username)
        {
            bool ret = true;
            User user = null;
            
            if (DataUsers.Users.TryGetValue(username, out user))
            {
                if (DateTime.Compare(user.LockedTime.AddMinutes(inActiveUser), DateTime.Now) <= 0)
                {
                    user.IsDisabled = true;
                    ret = false;
                    Console.WriteLine("User [{0}] is disabled because of inactivity....", username);
                }
                else
                {
                    if (user.IsDisabled)
                    {
                        Console.WriteLine("User [{0}] is disabled....", username);
                        ret = false;
                    }
                }
            }
            else
            {
                ret = false;
                Console.WriteLine("User with username [{0}] does not exists!", username);
            }

            return ret;
        }

        public bool UnAuthenticateUser(string username)
        {
            bool ret = true;
            User user = null;

            if (DataUsers.Users.TryGetValue(username, out user))
            {
                if (user.IsAuthenticated)
                {
                    user.IsAuthenticated = false;
                    user.LockedTime = DateTime.Now;
                    Console.WriteLine("User [{0}] is unauthenticated....", username);
                }
                else
                {
                    ret = false;
                    Console.WriteLine("User [{0}] is NOT authenticated!", username);
                }
            }
            else
            {
                ret = false;
                Console.WriteLine("User with username [{0}] does not exists!", username);
            }

            return ret;
        }

        private bool CheckPassword(User user, byte[] pass)
        {
            bool ret = false;

            if (pass.Length == user.Password.Length)
            {
                int i = 0;
                while ((i < pass.Length) && (pass[i] == user.Password[i]))
                {
                    i += 1;
                }
                if (i == pass.Length)
                {
                    ret = true;
                }
            }

            return ret;
        }
    }
}
