﻿using Common;
using System;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Security;
using Manager;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel.Description;

namespace CredentialsStore
{
    class Program
    {
        static void Main(string[] args)
        {
            string address = "net.tcp://localhost:10005/CheckUsers";
            string srvCertCN = Formatter.ParseName(WindowsIdentity.GetCurrent().Name);

            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate;
            ServiceHost host = new ServiceHost(typeof(CheckUsers));
            host.AddServiceEndpoint(typeof(ICheckUsers), binding, address);
            host.Credentials.ClientCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.Custom;
            host.Credentials.ClientCertificate.Authentication.CustomCertificateValidator = new ServiceCertValidator();
            host.Credentials.ClientCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;
            host.Credentials.ServiceCertificate.Certificate = CertManager.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, srvCertCN);


            string address1 = "net.tcp://localhost:10000/CredentialsStore";
            NetTcpBinding binding1 = new NetTcpBinding();
            binding1.Security.Mode = SecurityMode.Transport;
            binding1.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.EncryptAndSign;
            binding1.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;
            binding1.Security.Message.ClientCredentialType = MessageCredentialType.Windows;
            ServiceHost hostForAdmin = new ServiceHost(typeof(AccountManagement));
            hostForAdmin.AddServiceEndpoint(typeof(IAccountManagement), binding1, address1);
            hostForAdmin.Description.Behaviors.Remove(typeof(ServiceDebugBehavior));
            hostForAdmin.Description.Behaviors.Add(new ServiceDebugBehavior() { IncludeExceptionDetailInFaults = true });


            DataUsers.ReadFromXML();


            try
            {
                hostForAdmin.Open();
                host.Open();
                Console.WriteLine("CredentialsStore is started.\nPress <enter> to close service ...");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] {0}", e.Message);
                Console.WriteLine("[StackTrace] {0}", e.StackTrace);
            }
            finally
            {
                hostForAdmin.Close();
                host.Close();
                Console.WriteLine("CredentialsStore is closed. Press <enter> to exit...");
                Console.ReadLine();
            }
        }
    }
}
