﻿using System;
using System.Runtime.Serialization;

namespace Common
{
    [DataContract]
    public class User
    {
        #region Variables
        private string username;
        private byte[] password;
        private bool isAuthenticated;
        private bool isBlocked;
        private bool isDisabled;
        private short cntForLocking;
        private DateTime lockedTime;
        private bool adminBlockedUser;
        #endregion


        #region Props
        [DataMember]
        public string Username { get { return username; } set { username = value; } }

        [DataMember]
        public byte[] Password { get { return password; } set { password = value; } }

        [DataMember]
        public bool IsAuthenticated { get { return isAuthenticated; } set { isAuthenticated = value; } }

        [DataMember]
        public bool IsBlocked { get { return isBlocked; } set { isBlocked = value; } }

        [DataMember]
        public bool IsDisabled { get { return isDisabled; } set { isDisabled = value; } }

        [DataMember]
        public short CntForLocking { get { return cntForLocking; } set { cntForLocking = value; } }

        [DataMember]
        public DateTime LockedTime { get { return lockedTime; } set { lockedTime = value; } }

        [DataMember]
        public bool AdminBlockedUser { get { return adminBlockedUser; } set { adminBlockedUser = value; } }
        #endregion


        public User(string username, byte[] password)
        {
            this.username = username;
            this.password = new byte[password.Length];
            for(int i = 0; i < password.Length; i++)
            {
                this.password[i] = password[i];
            }
            isAuthenticated = false;
            isBlocked = false;
            isDisabled = false;
            cntForLocking = 2;
            lockedTime = DateTime.Now;
            adminBlockedUser = false;
        }

        public User()
        {
            isAuthenticated = false;
            isBlocked = false;
            isDisabled = false;
            cntForLocking = 2;
            lockedTime = DateTime.Now;
            adminBlockedUser = false;
        }
    }
}
