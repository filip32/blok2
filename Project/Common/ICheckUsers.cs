﻿using System.ServiceModel;

namespace Common
{
    [ServiceContract]
    public interface ICheckUsers
    {
        /// <summary>
        /// Function that is called by AuthService.
        /// Cheking if that user exists.
        /// Cheking if user is not blocked.
        /// Cheking how many times user tried to Log in
        /// Locking user if there is a need for that.
        /// </summary>
        /// <param name="username">to log in</param>
        /// <param name="password">to log in</param>
        /// <returns>true if user is successfully Authenticated otherwise false</returns>
        [OperationContract]
        bool AuthenticateUser(string username, byte[] password);

        /// <summary>
        /// Function that is cheking if current user that wants to log if he is Disabled
        /// </summary>
        /// <param name="username">used to check on user</param>
        /// <returns>false if user is disabled otherwise true</returns>
        [OperationContract]
        bool CheckIfDisabled(string username);

        /// <summary>
        /// Function that is loging out user
        /// </summary>
        /// <param name="username">used to log out</param>
        /// <returns>true if user is successfully unauthenticated otherwise false</returns>
        [OperationContract]
        bool UnAuthenticateUser(string username);
    }
}
