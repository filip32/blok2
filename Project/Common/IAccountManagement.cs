﻿using System.Collections.Generic;
using System.ServiceModel;

namespace Common
{
    [ServiceContract]
    public interface IAccountManagement
    {
        ///Functions called by AccountAdmins

        /// <summary>
        /// Creating a new account/user with params if there is no account/user with that username
        /// </summary>
        /// <param name="username">that is unique</param>
        /// <param name="password">for this user</param>
        /// <returns>true if user is successfully created otherwise false</returns>
        [OperationContract]
        bool CreateAccount(string username, byte[] password);

        /// <summary>
        /// Deleting existing account/user with username if user/account exists
        /// </summary>
        /// <param name="username">username of existing user to be deleted</param>
        /// <returns>true if user is successfully deleted otherwise false</returns>
        [OperationContract]
        bool DeleteAccount(string username);

        /// <summary>
        /// Locks account/user so that user can not log in if user/account exists
        /// </summary>
        /// <param name="username">username of user that is going to be locked</param>
        /// <returns>true if user is successfully locked otherwise false</returns>
        [OperationContract]
        bool LockAccount(string username);

        /// <summary>
        /// Enables account/user so that user can log in if user/account exists
        /// </summary>
        /// <param name="username">user that is going to be enabled</param>
        /// <returns>true if user is successfully enabled otherwise false</returns>
        [OperationContract]
        bool EnableAccount(string username);

        /// <summary>
        /// Disables account/user so that user can not log in if user/account exists
        /// </summary>
        /// <param name="username">user that is going to be disabled</param>
        /// <returns>true if user is successfully disabled otherwise false</returns>
        [OperationContract]
        bool DisableAccount(string username);

        /// <summary>
        /// Gets all users if there is any
        /// </summary>
        /// <returns>list with users if users exists, otherwise null</returns>
        [OperationContract]
        List<User> GetAllUsers();
    }
}
