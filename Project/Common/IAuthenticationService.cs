﻿using System.ServiceModel;

namespace Common
{
    [ServiceContract]
    public interface IAuthenticationService
    {
        /// <summary>
        /// Method that is called by AccountUser when user wants to Log In
        /// </summary>
        /// <param name="username">used to log in</param>
        /// <param name="password">used to log in</param>
        /// <returns>true if user is successfully Loged in otherwise false</returns>
        [OperationContract]
        bool LogIn(string username, byte[] password);

        /// <summary>
        /// Method that is called by AccountUser when user wants to Log out
        /// </summary>
        /// <param name="username">used to log out</param>
        /// <returns>true if user is successfully Loged out otherwise false</returns>
        [OperationContract]
        bool LogOut(string username);
    }
}
