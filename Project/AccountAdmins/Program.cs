﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace AccountAdmins
{
    class Program
    {
        static void Main(string[] args)
        {

            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.EncryptAndSign;
            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;
            binding.Security.Message.ClientCredentialType = MessageCredentialType.Windows;
            string address = "net.tcp://localhost:10000/CredentialsStore";

            using (AccountAdminProxy proxy = new AccountAdminProxy(binding, address))
            {
                string choice;
                bool operationCondition = true;

                Console.WriteLine("************************* Welcome to admin terminal *************************");
                while (operationCondition)
                {
                    Console.WriteLine(">> OPERATION MENU <<");
                    Console.WriteLine("1. Create account");
                    Console.WriteLine("2. Delete account");
                    Console.WriteLine("3. Lock account");
                    Console.WriteLine("4. Enable account");
                    Console.WriteLine("5. Disable account");
                    Console.WriteLine("6. Exit");
                    Console.WriteLine("\n Enter your choice >> ");
                    choice = Console.ReadLine();

                    switch (choice)
                    {
                        case "1":
                            AccountCreating(proxy);
                            break;
                        case "2":
                            AccountDeleting(proxy);
                            break;
                        case "3":
                            AccountLocking(proxy);
                            break;
                        case "4":
                            AccountEnabling(proxy);
                            break;
                        case "5":
                            AccountDisabling(proxy);
                            break;
                        case "6":
                            operationCondition = false;
                            break;
                        default:
                            Console.WriteLine("Your choice is invalid. Please try again.");
                            break;
                    }
                }
            }

            Console.ReadLine();
        }

        private static void AccountCreating(AccountAdminProxy proxy)
        {
            string username, password;

            Console.WriteLine("Enter your username >> ");
            username = Console.ReadLine();
            Console.WriteLine("Enter your password >> ");
            password = Console.ReadLine();

            proxy.CreateAccount(username,password);
        }

        private static void AccountDeleting(AccountAdminProxy proxy)
        {
            string username;
            List<User> users = proxy.Users();
            Console.WriteLine(">> Users list << ");

            int i = 0;
            foreach (User u in users)
            {
                i++;
                Console.WriteLine(i+". username >> " + u.Username);
            }

            Console.WriteLine("Enter username of user you want to delete >> ");
            username = Console.ReadLine();

            if (users.Where(xx => xx.Username == username) != null)
            {
                proxy.DeleteAccount(username);
            }
            else
            {
                Console.WriteLine("You have entered invalid username.");
            }

        }

        private static void AccountLocking(AccountAdminProxy proxy)
        {
            string username;
            List<User> users = proxy.Users();
            Console.WriteLine(">> Users list << ");

            int i = 0;
            foreach (User u in users)
            {
                i++;
                Console.WriteLine(i + ". username >> " + u.Username);
            }

            Console.WriteLine("Enter username of user you want to lock >> ");
            username = Console.ReadLine();

            if (users.Where(xx => xx.Username == username) != null)
            {
                proxy.LockAccount(username);
            }
            else
            {
                Console.WriteLine("You have entered invalid username.");
            }
        }

        private static void AccountEnabling(AccountAdminProxy proxy)
        {
            string username;
            List<User> users = proxy.Users();
            Console.WriteLine(">> Users list << ");

            int i = 0;
            foreach (User u in users)
            {
                if (u.IsDisabled)
                {
                    i++;
                    Console.WriteLine(i + ". username >> " + u.Username);
                }
            }

            Console.WriteLine("Enter username of user you want to enable >> ");
            username = Console.ReadLine();

            if (users.Where(xx => xx.Username == username) != null)
            {
                proxy.EnableAccount(username);
            }
            else
            {
                Console.WriteLine("You have entered invalid username.");
            }
        }

        private static void AccountDisabling(AccountAdminProxy proxy)
        {
            string username;
            List<User> users = proxy.Users();
            Console.WriteLine(">> Users list << ");

            int i = 0;
            foreach (User u in users)
            {
                if (!u.IsDisabled)
                {
                    i++;
                    Console.WriteLine(i + ". username >> " + u.Username);
                }
            }

            Console.WriteLine("Enter username of user you want to disable >> ");
            username = Console.ReadLine();

            if (users.Where(xx => xx.Username == username) != null)
            {
                proxy.DisableAccount(username);
            }
            else
            {
                Console.WriteLine("You have entered invalid username.");
            }
        }

    }
}
