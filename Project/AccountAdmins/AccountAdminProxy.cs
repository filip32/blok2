﻿using Common;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;

namespace AccountAdmins
{
    public class AccountAdminProxy: ChannelFactory<IAccountManagement>, IDisposable
    {
        IAccountManagement factory;

        public AccountAdminProxy(NetTcpBinding binding, string address) : base(binding, address)
        {
            factory = this.CreateChannel();
        }

        public void CreateAccount(string username, string password)
        {
            bool result = false;
            try
            {
                byte[] pass;
                using (var sha = new SHA1Managed())
                {
                    pass = sha.ComputeHash(Encoding.UTF8.GetBytes(password));
                }

                result = factory.CreateAccount(username, pass);

                if (result)
                {
                    Console.WriteLine("Account successfully created.");
                }
                else
                {
                    Console.WriteLine("Failed to create an account.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
        }

        public void DeleteAccount(string username)
        {
            bool result = false;
            try
            {
                result = factory.DeleteAccount(username);
                if (result)
                {
                    Console.WriteLine("Account successfully deleted.");
                }
                else
                {
                    Console.WriteLine("Failed to delete an account.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
        }

        public void LockAccount(string username)
        {
            bool result = false;
            try
            {
                result = factory.LockAccount(username);
                if (result)
                {
                    Console.WriteLine("Account successfully locked.");
                }
                else
                {
                    Console.WriteLine("Failed to lock an account.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
        }

        public void EnableAccount(string username)
        {
            bool result = false;
            try
            {
                result = factory.EnableAccount(username);

                if (result)
                {
                    Console.WriteLine("Account successfully enabled.");
                }
                else
                {
                    Console.WriteLine("Failed to enable an account.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
        }

        public void DisableAccount(string username)
        {
            bool result = false;
            try
            {
                result = factory.DisableAccount(username);
                if (result)
                {
                    Console.WriteLine("Account successfully disabled.");
                }
                else
                {
                    Console.WriteLine("Failed to disabled an account.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
        }

        public List<User> Users()
        {
            List<User> users = new List<User>();
            try
            {
                users = factory.GetAllUsers();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }

            return users;
        }

    }
}
