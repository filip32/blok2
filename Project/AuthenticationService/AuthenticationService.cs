﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.ServiceModel;
using Manager;
using System.Security.Cryptography.X509Certificates;

namespace AuthenticationService
{
	public class AuthenticationService : ChannelFactory<ICheckUsers>, IAuthenticationService, IDisposable
	{
		public static Dictionary<string, byte[]> loginUsers = new Dictionary<string, byte[]>();
		public ICheckUsers factory;

		public AuthenticationService()
		{
			ConnectToStore();
		}

		public void ConnectToStore()
		{
			string srvCertCN = "CredentialStore";

			NetTcpBinding binding = new NetTcpBinding();
			binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate;

			/// Use CertManager class to obtain the certificate based on the "srvCertCN" representing the expected service identity.
			X509Certificate2 srvCert = CertManager.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, srvCertCN);

			EndpointAddress address = new EndpointAddress(new Uri("net.tcp://10.1.212.172:10005/CheckUsers"), new X509CertificateEndpointIdentity(srvCert));

			ChannelFactory<ICheckUsers> ff = new ChannelFactory<ICheckUsers>(binding, address);

			string cltCertCN = Formatter.ParseName(WindowsIdentity.GetCurrent().Name);

			ff.Credentials.ServiceCertificate.Authentication.CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.Custom;
			ff.Credentials.ServiceCertificate.Authentication.CustomCertificateValidator = new ClientCertValidator();
			ff.Credentials.ServiceCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;

			/// Set appropriate client's certificate on the channel. Use CertManager class to obtain the certificate based on the "cltCertCN"
			ff.Credentials.ClientCertificate.Certificate = CertManager.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, cltCertCN);

			factory = ff.CreateChannel();
		}

		public bool LogIn(string username, byte[] password)
		{
			bool isTrue = false;

			if (!loginUsers.Keys.Contains(username))
			{
				try
				{
					if (factory.CheckIfDisabled(username))
					{
						if (factory.AuthenticateUser(username, password))
						{
							Console.WriteLine("User [{0}] is authenticated....", username);
							loginUsers.Add(username, password);
							isTrue = true;
						}
						else
							Console.WriteLine("User [{0}] is NOT authenticated because it is blocked....", username);
					}
					else
						Console.WriteLine("User [{0}] is disabled of inactivity....", username);

				}
				catch (Exception ex)
				{
					isTrue = false;
					Console.WriteLine(ex.Message);
					throw new Exception(ex.Message);
				}
			}
			else
				Console.WriteLine("User [{0}] does NOT exists!", username);

			return isTrue;
		}

		public bool LogOut(string username)
		{
			bool isTrue = false;

			if (loginUsers.Keys.Contains(username))
			{
				try
				{
					if (factory.UnAuthenticateUser(username))
					{
						loginUsers.Remove(username);
						isTrue = true;
						Console.WriteLine("User [{0}] is unauthenticated....", username);
					}
					else
						Console.WriteLine("User [{0}] is NOT unauthenticated....", username);
				}
				catch (Exception ex)
				{
					isTrue = false;
					Console.WriteLine(ex.Message);
					throw new Exception(ex.Message);
				}
			}
			else
				Console.WriteLine("User [{0}] does NOT exists!", username);

			return isTrue;
		}
	}
}
