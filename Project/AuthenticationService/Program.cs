﻿using Common;
using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace AuthenticationService
{
    class Program
    {
        static void Main(string[] args)
        {
            NetTcpBinding bindingForUser = new NetTcpBinding();
            bindingForUser.Security.Mode = SecurityMode.Transport;
            bindingForUser.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.EncryptAndSign;
            bindingForUser.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;

            string addressForUser = "net.tcp://localhost:10001/AuthenticationService";

            ServiceHost host = new ServiceHost(typeof(AuthenticationService));
            host.AddServiceEndpoint(typeof(IAuthenticationService), bindingForUser, addressForUser);
            host.Description.Behaviors.Remove(typeof(ServiceDebugBehavior));
            host.Description.Behaviors.Add(new ServiceDebugBehavior() { IncludeExceptionDetailInFaults = true });

            try
            {
                host.Open();
                Console.WriteLine("Authentication Service is started.\nPress <enter> to close service...");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] {0}", e.Message);
                Console.WriteLine("[StackTrace] {0}", e.StackTrace);
            }
            finally
            {
                host.Close();
                Console.WriteLine("AuthenticationService is closed. Press <enter> to exit...");
                Console.ReadLine();
            }
        }
    }
}
