﻿using System;
using System.ServiceModel;

namespace AccountUser
{
    class Program
    {
        static void Main(string[] args)
        {
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Mode = SecurityMode.Transport;
            binding.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.EncryptAndSign;
            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;
            string address = "net.tcp://localhost:10001/AuthenticationService";
            string username, password;
            bool loggedIn = false;
            Console.WriteLine("************************* Welcome to user terminal *************************");

            using (AccountUserProxy proxy = new AccountUserProxy(binding, address))
            {
                while (!loggedIn)
                {
                    Console.WriteLine("Please enter your username >> ");
                    username = Console.ReadLine();
                    Console.WriteLine("Please enter your password >> ");
                    password = Console.ReadLine();
                    loggedIn = proxy.Login(username,password);
                }

                Console.WriteLine("Press any key to log out >> ");
                Console.ReadLine();
                proxy.LogOut();
            }

        }
    }
}
