﻿using Common;
using System;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;

namespace AccountUser
{
    public class AccountUserProxy: ChannelFactory<IAuthenticationService>, IDisposable
    {
        IAuthenticationService factory;
        string username;

        public AccountUserProxy(NetTcpBinding binding, string address) : base(binding, address)
        {
            factory = this.CreateChannel();
        }

        public bool Login(string username, string password)
        {
            bool result = false;
            try
            {
                byte[] pass;
                using (var sha = new SHA1Managed())
                {
                    pass = sha.ComputeHash(Encoding.UTF8.GetBytes(password));
                }

                result = factory.LogIn(username, pass);

                if (result)
                {
                    Console.WriteLine("User successfully logged in.");
                    this.username = username;
                    return true;
                }
                else
                {
                    Console.WriteLine("Failed log in.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
            return false;
        }

        public bool LogOut()
        {
            bool result = false;
            try
            {
                result = factory.LogOut(username);
                if (result)
                {
                    Console.WriteLine("Successfully logged out.");
                    return true;
                }
                else
                {
                    Console.WriteLine("Failed to log out from account.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
            return false;
        }
    }
}
